FROM node:18
WORKDIR /usr/src/app
COPY ./nodeServer/package*.json ./
RUN npm install
# If you are building your code for production
# RUN npm ci --omit=dev
# Bundle app source
COPY ./nodeServer/server.js  .
EXPOSE 8080
CMD [ "node", "server.js" ]